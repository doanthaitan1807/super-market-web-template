﻿namespace Lab10._3.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Product")]
    public partial class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DisplayName("Mã rượu:")]
        public int ProductID { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Mã rượu:")]
        public string ProductName { get; set; }

        [Column(TypeName = "text")]
        [DisplayName("Mô tả:")]
        public string Description { get; set; }

        [Column(TypeName = "numeric")]
        [DisplayName("Giá nhập::")]
        public decimal PurchasePrice { get; set; }

        [Column(TypeName = "numeric")]
        [DisplayName("Giá bán::")]
        public decimal Price { get; set; }

        [DisplayName("")]
        public int Quantity { get; set; }

        [StringLength(20)]
        [DisplayName("Năm sản xuất:")]
        public string Vintage { get; set; }

        [Required]
        [StringLength(10)]
        public string CatalogyID { get; set; }

        [Column(TypeName = "text")]
        public string Image { get; set; }

        [Required]
        [StringLength(100)]
        public string Region { get; set; }

        public virtual Catalogy Catalogy { get; set; }
    }
}
